v {xschem version=3.1.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 330 -730 330 -690 {
lab=enb}
N 260 -760 290 -760 {
lab=en}
N 260 -760 260 -660 {
lab=en}
N 260 -660 290 -660 {
lab=en}
N 220 -710 260 -710 {
lab=en}
N 330 -710 390 -710 {
lab=enb}
N 330 -820 330 -790 {
lab=AVDD}
N 330 -760 390 -760 {
lab=AVDD}
N 330 -660 390 -660 {
lab=VSS}
N 330 -630 330 -600 {
lab=VSS}
N 750 -850 750 -830 {
lab=AVDD}
N 670 -800 750 -800 {
lab=AVDD}
N 930 -850 930 -830 {
lab=AVDD}
N 790 -800 890 -800 {
lab=vdiode}
N 930 -800 1020 -800 {
lab=AVDD}
N 750 -770 750 -590 {
lab=vout}
N 930 -770 930 -660 {
lab=vdiode}
N 840 -800 840 -730 {
lab=vdiode}
N 840 -730 930 -730 {
lab=vdiode}
N 550 -590 550 -550 {
lab=vout}
N 550 -590 840 -590 {
lab=vout}
N 550 -520 570 -520 {
lab=VSS}
N 490 -520 510 -520 {
lab=vin2}
N 690 -520 710 -520 {
lab=VSS}
N 750 -520 760 -520 {
lab=vip2}
N 910 -520 920 -520 {
lab=vin1}
N 960 -520 990 -520 {
lab=VSS}
N 1090 -520 1120 -520 {
lab=VSS}
N 1160 -520 1190 -520 {
lab=vip1}
N 550 -490 550 -450 {
lab=vsin2}
N 710 -490 710 -450 {
lab=vsip2}
N 960 -490 960 -450 {
lab=vsin1}
N 1120 -490 1120 -450 {
lab=vsip1}
N 840 -590 900 -590 {
lab=vout}
N 760 -520 780 -520 {
lab=vip2}
N 890 -520 910 -520 {
lab=vin1}
N 960 -590 960 -550 {
lab=vout}
N 900 -590 960 -590 {
lab=vout}
N 930 -660 930 -620 {
lab=vdiode}
N 710 -620 930 -620 {
lab=vdiode}
N 710 -620 710 -550 {
lab=vdiode}
N 930 -620 1120 -620 {
lab=vdiode}
N 1120 -620 1120 -550 {
lab=vdiode}
N 1400 -390 1450 -390 {
lab=VSS}
N 1400 -450 1400 -420 {
lab=VSS}
N 1330 -390 1360 -390 {
lab=VSS}
N 1400 -360 1400 -330 {
lab=VSS}
N 1050 -260 1100 -260 {
lab=VSS}
N 1050 -320 1050 -290 {
lab=vs1}
N 980 -260 1010 -260 {
lab=vg_cs}
N 1050 -230 1050 -200 {
lab=VSS}
N 740 -250 790 -250 {
lab=VSS}
N 670 -250 700 -250 {
lab=vg_cs}
N 740 -220 740 -190 {
lab=VSS}
N 740 -310 740 -280 {
lab=vs2}
N 590 -160 640 -160 {
lab=VSS}
N 590 -220 590 -190 {
lab=vg_cs}
N 520 -160 550 -160 {
lab=enb}
N 590 -130 590 -100 {
lab=VSS}
N 590 -250 670 -250 {
lab=vg_cs}
N 590 -250 590 -220 {
lab=vg_cs}
N 380 -340 430 -340 {
lab=vg_cs}
N 430 -340 430 -170 {
lab=vg_cs}
N 380 -170 430 -170 {
lab=vg_cs}
N 430 -250 470 -250 {
lab=vg_cs}
N 280 -340 320 -340 {
lab=vbias}
N 280 -340 280 -170 {
lab=vbias}
N 280 -170 320 -170 {
lab=vbias}
N 470 -250 590 -250 {
lab=vg_cs}
N 350 -410 350 -380 {
lab=enb}
N 350 -130 350 -90 {
lab=en}
N 240 -250 280 -250 {
lab=vbias}
N 120 -220 120 -180 {
lab=VSS}
N 160 -250 180 -250 {
lab=vbias}
N 60 -250 120 -250 {
lab=VSS}
N 120 -330 120 -280 {
lab=vbias}
N 180 -250 240 -250 {
lab=vbias}
N 120 -330 210 -330 {
lab=vbias}
N 210 -330 210 -250 {
lab=vbias}
N 120 -360 120 -330 {
lab=vbias}
N 120 -70 120 -30 {
lab=VSS}
N 60 -100 120 -100 {
lab=VSS}
N 120 -150 120 -130 {
lab=vbias}
N 120 -150 240 -150 {
lab=vbias}
N 240 -250 240 -150 {
lab=vbias}
N 160 -100 240 -100 {
lab=vbias}
N 240 -150 240 -100 {
lab=vbias}
N 350 -190 350 -170 {
lab=VSS}
N 350 -340 350 -310 {
lab=#net1}
C {devices/iopin.sym} 30 -730 0 0 {name=p1 lab=AVDD}
C {devices/iopin.sym} 30 -710 0 0 {name=p2 lab=VSS}
C {devices/iopin.sym} 30 -670 0 0 {name=p3 lab=vip1}
C {devices/iopin.sym} 30 -650 0 0 {name=p4 lab=vin1}
C {devices/iopin.sym} 30 -630 0 0 {name=p5 lab=vip2}
C {devices/iopin.sym} 30 -610 0 0 {name=p6 lab=vin2}
C {devices/iopin.sym} 30 -590 0 0 {name=p7 lab=vout}
C {devices/iopin.sym} 30 -550 0 0 {name=p8 lab=en}
C {devices/iopin.sym} 30 -530 0 0 {name=p9 lab=vdiode}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 310 -760 0 0 {name=en_inv_pmos
w=500n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 310 -660 0 0 {name=en_inv_nmos
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 330 -820 0 0 {name=l1 sig_type=std_logic lab=AVDD}
C {devices/lab_pin.sym} 390 -760 2 0 {name=l2 sig_type=std_logic lab=AVDD}
C {devices/lab_pin.sym} 220 -710 0 0 {name=l3 sig_type=std_logic lab=en}
C {devices/lab_pin.sym} 390 -710 2 0 {name=l4 sig_type=std_logic lab=enb}
C {devices/lab_pin.sym} 390 -660 2 0 {name=l5 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 330 -600 2 0 {name=l6 sig_type=std_logic lab=VSS}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 770 -800 0 1 {name=load_n
w=500n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 750 -850 0 0 {name=l7 sig_type=std_logic lab=AVDD}
C {devices/lab_pin.sym} 670 -800 0 0 {name=l8 sig_type=std_logic lab=AVDD}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 910 -800 0 0 {name=load_p
w=500n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 930 -850 0 0 {name=l9 sig_type=std_logic lab=AVDD}
C {devices/lab_pin.sym} 1020 -800 2 0 {name=l10 sig_type=std_logic lab=AVDD}
C {devices/lab_pin.sym} 840 -750 0 0 {name=l11 sig_type=std_logic lab=vdiode}
C {devices/lab_pin.sym} 750 -700 0 0 {name=l12 sig_type=std_logic lab=vout}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 530 -520 0 0 {name=amp_n2
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 730 -520 0 1 {name=amp_p2
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 940 -520 0 0 {name=amp_n1
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 1140 -520 0 1 {name=amp_p1
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/iopin.sym} 550 -460 1 0 {name=p10 lab=vsin2}
C {devices/iopin.sym} 710 -460 1 0 {name=p11 lab=vsip2}
C {devices/iopin.sym} 960 -460 1 0 {name=p12 lab=vsin1}
C {devices/iopin.sym} 1120 -460 1 0 {name=p13 lab=vsip1}
C {devices/lab_pin.sym} 490 -520 0 0 {name=l13 sig_type=std_logic lab=vin2}
C {devices/lab_pin.sym} 570 -520 2 0 {name=l14 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 690 -520 0 0 {name=l15 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 780 -520 2 0 {name=l16 sig_type=std_logic lab=vip2}
C {devices/lab_pin.sym} 890 -520 0 0 {name=l17 sig_type=std_logic lab=vin1}
C {devices/lab_pin.sym} 990 -520 2 0 {name=l18 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1090 -520 0 0 {name=l19 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1190 -520 2 0 {name=l20 sig_type=std_logic lab=vip1}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 1380 -390 0 0 {name=XDUMMY
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 1450 -390 2 0 {name=l21 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1400 -330 2 0 {name=l22 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1330 -390 0 0 {name=l23 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1400 -450 0 0 {name=l24 sig_type=std_logic lab=VSS}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 1030 -260 0 0 {name=cs1
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 1100 -260 2 0 {name=l25 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1050 -200 2 0 {name=l26 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 980 -260 0 0 {name=l27 sig_type=std_logic lab=vg_cs}
C {devices/iopin.sym} 1050 -310 3 0 {name=p14 lab=vs1}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 720 -250 0 0 {name=cs2
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 790 -250 2 0 {name=l28 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 740 -190 2 0 {name=l29 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 670 -250 1 0 {name=l30 sig_type=std_logic lab=vg_cs}
C {devices/iopin.sym} 740 -300 3 0 {name=p15 lab=vs2}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 570 -160 0 0 {name=Sw_enb
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 640 -160 2 0 {name=l31 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 590 -100 2 0 {name=l32 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 520 -160 0 0 {name=l33 sig_type=std_logic lab=enb}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 350 -360 1 0 {name=Sw_en_pmos
w=500n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 350 -150 3 0 {name=Sw_en_nmos
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 350 -410 0 0 {name=l34 sig_type=std_logic lab=enb}
C {devices/lab_pin.sym} 350 -90 2 0 {name=l35 sig_type=std_logic lab=en}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 140 -250 0 1 {name=cs_diode_l
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 120 -180 2 1 {name=l37 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 60 -250 2 1 {name=l36 sig_type=std_logic lab=VSS}
C {devices/iopin.sym} 120 -350 3 0 {name=p16 lab=vbias}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 140 -100 0 1 {name=cs_diode_r
w=500n
l=90n
nf=4
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 120 -30 2 1 {name=l38 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 60 -100 2 1 {name=l39 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 350 -190 3 1 {name=l40 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 350 -310 3 0 {name=l41 sig_type=std_logic lab=AVDD}
