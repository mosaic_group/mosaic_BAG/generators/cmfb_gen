from typing import *
from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackID, TrackManager

from sal.row import *
from sal.transistor import *
from sal.routing_grid import RoutingGridHelper

from .params import cmfb_layout_params


class layout(AnalogBase):
    """
    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params="cmfb_layout_params parameter object"
        )

    def draw_layout(self):
        params: cmfb_layout_params = self.params['params']

        # Define what layers will be used for horizontal and vertical connections
        # horiz_conn_layer = self.mos_conn_layer + 1
        vert_conn_layer = self.mos_conn_layer + 2

        ################################################################################
        # 0:   Floorplan the design
        #
        # Know where transistors go and where the horizontal
        # and vertical connections will be
        # Also determine how transistors will be aligned horizontally relative to eachother
        # and know how things will need to shift as various sizes scale
        ################################################################################

        ################################################################################
        # 1:   Set up track allocations for each row
        #
        # Use TrackManager to create allocate track and spacing based on the floorplan
        #   -  Rather than explicitly allocate a number of gate and source/drain tracks for each row (as is done in
        #      the bootcamp modules), instead define a list of what horizontal connects will be required for each row.
        #   -  Based on spacing and width rules provided in the specification file, BAG/trackmanager will calculate
        #      how many tracks each row needs
        # Finally, initialize the rows using the helper functions above
        ################################################################################
        tr_manager = TrackManager(grid=self.grid, tr_widths=params.tr_widths, tr_spaces=params.tr_spaces)

        # Rows are ordered from bottom to top
        # To use TrackManager, an ordered list of wiring types and their locations must be provided.
        # Define two lists, one for the nch rows and one for the pch rows
        # The lists are composed of dictionaries, one per row.
        # Each dictionary has two list entries (g and ds), which are ordered lists of what wire types will be present
        #  in the g and ds sections of that row. Ordering is from bottom to top of the design.

        # Set up the row information
        # Row information contains the row properties like width/number of fins, orientation, intent, etc.
        # Storing in a row dictionary/object allows for convenient fetching of data in later functions
        row_Dummy_NB = Row(name='Dummy_NB',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NB'],
                           threshold=params.th_dict['Dummy_NB'],
                           wire_names_g=['dum'],
                           wire_names_ds=['dum', 'sig1']
                           )
        row_tail_B = Row(name='tail_B',
                         orientation=RowOrientation.R0,
                         channel_type=ChannelType.N,
                         width=params.w_dict['tail_B'],
                         threshold=params.th_dict['tail_B'],
                         wire_names_g=['dum', 'vg_cs', 'vbias'],
                         wire_names_ds=['vs1', 'vs2', 'sig1']
                         )
        row_tail_T = Row(name='tail_T',
                         orientation=RowOrientation.R0,
                         channel_type=ChannelType.N,
                         width=params.w_dict['tail_T'],
                         threshold=params.th_dict['tail_T'],
                         wire_names_g=['vg_cs', 'vbias'],
                         wire_names_ds=['vs', 'sig1']
                         )
        row_in = Row(name='In',
                     orientation=RowOrientation.R0,
                     channel_type=ChannelType.N,
                     width=params.w_dict['In'],
                     threshold=params.th_dict['In'],
                     wire_names_g=['dum', 'cm', 'IN'],
                     wire_names_ds=['vout', 'vsip1', 'vsip2']
                     )
        row_Dummy_NT = Row(name='Dummy_NT',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NT'],
                           threshold=params.th_dict['Dummy_NT'],
                           wire_names_g=['dum', 'dum'],
                           wire_names_ds=['vs2', 'sig1']
                           )

        row_Dummy_PB = Row(name='Dummy_PB',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.P,
                           width=params.w_dict['Dummy_PB'],
                           threshold=params.th_dict['Dummy_PB'],
                           wire_names_g=['sig1', 'vdiod'],
                           wire_names_ds=['sig1']
                           )
        row_load = Row(name='load',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.P,
                       width=params.w_dict['load'],
                       threshold=params.th_dict['load'],
                       wire_names_g=['sig1', 'vdiod'],
                       wire_names_ds=['sw', 'sig1']
                       )
        row_Dummy_PT = Row(name='Dummy_PT',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.P,
                           width=params.w_dict['Dummy_PT'],
                           threshold=params.th_dict['Dummy_PT'],
                           wire_names_g=['sig1'],
                           wire_names_ds=['sw', 'sig1']
                           )

        # Define the order of the rows (bottom to top) for this analogBase cell
        rows = RowList([row_Dummy_NB, row_tail_B, row_tail_T, row_in, row_Dummy_NT,
                        row_Dummy_PB, row_load, row_Dummy_PT])

        ################################################################################
        # 2:
        # Initialize the transistors in the design
        # Storing each transistor's information (name, location, row, size, etc) in a dictionary object allows for
        # convient use later in the code, and also greatly simplifies the schematic generation
        # The initialization sets the transistor's row, width, and source/drain net names for proper dummy creation
        ################################################################################

        fg_cs = params.seg_dict['cs'] // 2
        fg_cs_diod = params.seg_dict['cs_diod'] // 2
        fg_amp = params.seg_dict['amp']
        fg_load = params.seg_dict['load']
        fg_sw_en = params.seg_dict['sw_en']
        fg_en_inv = params.seg_dict['en_inv']

        cs1_B = Transistor(name='cs1_B', row=row_tail_B, fg=fg_cs, deff_net='vs1')
        cs1_T = Transistor(name='cs1_T', row=row_tail_T, fg=fg_cs, deff_net='vs1')
        cs2_B = Transistor(name='cs2_B', row=row_tail_B, fg=fg_cs, deff_net='vs2')
        cs2_T = Transistor(name='cs2_T', row=row_tail_T, fg=fg_cs, deff_net='vs2')
        cs_diod_B = Transistor(name='cs_diod_B', row=row_tail_B, fg=fg_cs_diod, deff_net='vbias')
        cs_diod_T = Transistor(name='cs_diod_T', row=row_tail_T, fg=fg_cs_diod, deff_net='vbias')

        amp_n1 = Transistor(name='amp_n1', row=row_in, fg=fg_amp, deff_net='vout', seff_net='vsin1')
        amp_n2 = Transistor(name='amp_n2', row=row_in, fg=fg_amp, deff_net='vout', seff_net='vsin2')
        amp_p1 = Transistor(name='amp_p1', row=row_in, fg=fg_amp, deff_net='vdiode', seff_net='vsip1')
        amp_p2 = Transistor(name='amp_p2', row=row_in, fg=fg_amp, deff_net='vdiode', seff_net='vsip2')

        load_n = Transistor(name='load_n', row=row_load, fg=fg_load, deff_net='vout')
        load_p = Transistor(name='load_p', row=row_load, fg=fg_load, deff_net='vdiode')

        sw_en = Transistor(name='sw_en', row=row_tail_B, fg=fg_sw_en, deff_net='vg_cs')
        sw_en_n = Transistor(name='sw_en_n', row=row_in, fg=fg_sw_en, deff_net='vbias', seff_net='vg_cs')
        sw_en_p = Transistor(name='sw_en_p', row=row_load, fg=fg_sw_en, deff_net='vbias', seff_net='vg_cs')

        inv_n = Transistor(name='inv_n', row=row_in, fg=fg_en_inv, deff_net='enb')
        inv_p = Transistor(name='inv_p', row=row_load, fg=fg_en_inv, deff_net='enb')

        # Compose a list of all the transistors so it can be iterated over later
        transistors = [
            cs1_B, cs1_T, cs2_B, cs2_T, cs_diod_B, cs_diod_T, amp_n1, amp_n2, amp_p1, amp_p2,
            load_n, load_p, sw_en, sw_en_n, sw_en_p, inv_n, inv_p,
        ]

        ################################################################################
        # 3:   Calculate transistor locations
        # Based on the floorplan, want the tail, input, nmos regen, pmos regen, and pmos tail to be in a column
        # and for convenience, place the reset and load in a column, but right/left justified
        # Notation:
        #     fg_xxx refers to how wide (in fingers) a transistor or column of transistors is
        #     col_xxx refers to the location of the left most finger of a transistor or a column of transistors
        ################################################################################
        fg_st1 = max(cs1_B.fg, 2 * amp_n1.fg, load_n.fg)
        fg_st2 = max(sw_en.fg, sw_en_n.fg, sw_en_p.fg)
        fg_st3 = max(inv_p.fg, inv_n.fg, cs_diod_B.fg)

        # Add an explicit gap in the middle for symmetry. Set to 0 to not have gap
        fg_mid = 0

        # Get the minimum gap between fingers of different transistors
        # This varies between processes, so avoid hard-coding by using the method in self._tech_cls
        fg_space = self._tech_cls.get_min_fg_sep(params.lch)

        fg_total = params.fg_dum + fg_st3 + fg_space + fg_st2 + fg_space + fg_st1 * 2 + params.fg_dum

        # Calculate the starting column index for each stack of transistors
        col_st3_left = params.fg_dum
        col_st2_left = params.fg_dum + fg_st3 + fg_space
        col_st1_left = col_st2_left + fg_st2 + fg_space
        col_st1_right = col_st1_left + fg_st1 + fg_space

        # Calculate positions of transistors
        # This uses helper functions to place each transistor within a stack/column of a specified starting index and
        # width, and with a certain alignment (left, right, centered) within that column
        inv_p.assign_column(offset=col_st3_left, fg_col=fg_st3, align=TransistorAlignment.CENTER)
        inv_n.assign_column(offset=col_st3_left, fg_col=fg_st3, align=TransistorAlignment.CENTER)
        sw_en.assign_column(offset=col_st2_left, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        sw_en_p.assign_column(offset=col_st2_left, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        sw_en_n.assign_column(offset=col_st2_left, fg_col=fg_st2, align=TransistorAlignment.CENTER)

        load_n.assign_column(offset=col_st1_left, fg_col=fg_st1, align=TransistorAlignment.CENTER)
        load_p.assign_column(offset=col_st1_right, fg_col=fg_st1, align=TransistorAlignment.CENTER)

        amp_n2.assign_column(offset=col_st1_left, fg_col=fg_st1 // 2, align=TransistorAlignment.RIGHT)
        amp_n1.assign_column(offset=col_st1_left + fg_st1 // 2, fg_col=fg_st1 // 2, align=TransistorAlignment.LEFT)
        amp_p2.assign_column(offset=col_st1_right, fg_col=fg_st1 // 2, align=TransistorAlignment.RIGHT)
        amp_p1.assign_column(offset=col_st1_right + fg_st1 // 2, fg_col=fg_st1 // 2, align=TransistorAlignment.LEFT)

        cs_diod_T.assign_column(offset=col_st3_left, fg_col=fg_st3, align=TransistorAlignment.CENTER)
        cs_diod_B.assign_column(offset=col_st3_left, fg_col=fg_st3, align=TransistorAlignment.CENTER)

        cs2_T.assign_column(offset=col_st1_left, fg_col=fg_st1, align=TransistorAlignment.RIGHT)
        cs2_B.assign_column(offset=col_st1_left, fg_col=fg_st1, align=TransistorAlignment.RIGHT)
        cs1_T.assign_column(offset=col_st1_right, fg_col=fg_st1, align=TransistorAlignment.LEFT)
        cs1_B.assign_column(offset=col_st1_right, fg_col=fg_st1, align=TransistorAlignment.LEFT)

        ################################################################################
        # 4:  Assign the transistor directions (s/d up vs down)
        #
        # Specify the directions that connections to the source and connections to the drain will go (up vs down)
        # Doing so will also determine how the gate is aligned (ie will it be aligned to the source or drain)
        # See the bootcamp for more details
        # The helper functions used here help to abstract away whether the intended source/drain diffusion region of
        # a transistor occurs on the even or odd columns of that device (BAG always considers the even columns of a
        # device to be the 's').
        # These helper functions allow a user to specify whether the even columns should be the transistors effective
        #  source or effective drain, so that the user does not need to worry about BAG's notation.
        ################################################################################

        # Set tail transistor to have source on the leftmost diffusion (arbitrary) and source going down
        # inv_n.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        # inv_p.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)
        sw_en.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        sw_en_n.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        sw_en_p.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)

        amp_n1.set_directions(seff=EffectiveSource.D, seff_dir=TransistorDirection.MIDDLE)
        amp_n2.set_directions(seff=EffectiveSource.D, seff_dir=TransistorDirection.MIDDLE)
        amp_p1.set_directions(seff=EffectiveSource.D, seff_dir=TransistorDirection.MIDDLE)
        amp_p2.set_directions(seff=EffectiveSource.D, seff_dir=TransistorDirection.MIDDLE)

        cs1_B.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        cs1_T.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        cs2_B.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        cs2_T.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)

        cs_diod_B.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        cs_diod_T.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)

        load_p.set_matched_direction(reference=amp_p2, seff_dir=TransistorDirection.UP, aligned=False)
        load_n.set_matched_direction(reference=amp_n2, seff_dir=TransistorDirection.UP, aligned=False)
        inv_n.set_matched_direction(reference=cs_diod_T, seff_dir=TransistorDirection.DOWN, aligned=True)
        inv_p.set_matched_direction(reference=inv_n, seff_dir=TransistorDirection.UP, aligned=True)

        ################################################################################
        # 5:  Draw the transistor rows, and the transistors
        #
        # All the difficult setup has been complete. Drawing the transistors is simple now.
        # Note that we pass the wire_names dictionary defined above so that BAG knows how to space out
        # the transistor rows. BAG uses this to calculate how many tracks to allocate to each
        ################################################################################

        n_rows = rows.n_rows
        p_rows = rows.p_rows

        # Draw the transistor row bases
        self.draw_base(params.lch, fg_total, params.ptap_w, params.ntap_w,
                       n_rows.attribute_values('width'), n_rows.attribute_values('threshold'),
                       p_rows.attribute_values('width'), p_rows.attribute_values('threshold'),
                       tr_manager=tr_manager, wire_names=rows.wire_names_dict(),
                       n_orientations=n_rows.attribute_values('orientation'),
                       p_orientations=p_rows.attribute_values('orientation'),
                       top_layer=params.top_layer,
                       half_blk_x=True, half_blk_y=True,
                       guard_ring_nf=params.guard_ring_nf,
                       )

        # Draw the transistors
        for tx in transistors:
            ports = self.draw_mos_conn(mos_type=tx.row.channel_type.value,
                                       row_idx=rows.index_of_same_channel_type(tx.row),
                                       col_idx=tx.col,
                                       fg=tx.fg,
                                       sdir=tx.s_dir.value,
                                       ddir=tx.d_dir.value,
                                       s_net=tx.s_net,
                                       d_net=tx.d_net,
                                       gate_ext_mode=1,
                                       g_via_row=2,
                                       )
            tx.set_ports(g=ports['g'],
                         d=ports[tx.deff.value],
                         s=ports[tx.seff.value])

        ################################################################################
        # 6:  Define horizontal tracks on which connections will be made
        #
        # Based on the wire_names dictionary defined in step 1), create TrackIDs on which horizontal connections will
        #  be made
        ################################################################################

        row_tail_B_index = rows.index_of_same_channel_type(row_tail_B)
        row_tail_T_index = rows.index_of_same_channel_type(row_tail_T)
        row_in_index = rows.index_of_same_channel_type(row_in)
        row_load_index = rows.index_of_same_channel_type(row_load)

        tid_vbias = self.get_wire_id('nch', row_tail_B_index, 'g', wire_name='vbias')
        tid_B_vg_cs = self.get_wire_id('nch', row_tail_B_index, 'g', wire_name='vg_cs')
        tid_T_vg_cs = self.get_wire_id('nch', row_tail_T_index, 'g', wire_name='vg_cs')
        tid_tail_B_vs1 = self.get_wire_id('nch', row_tail_B_index, 'ds', wire_name='vs1')
        tid_tail_B_vs2 = self.get_wire_id('nch', row_tail_B_index, 'ds', wire_name='vs2')
        tid_tail_T_vs = self.get_wire_id('nch', row_tail_T_index, 'ds', wire_name='vs')

        tid_vout = self.get_wire_id('nch', row_in_index, 'ds', wire_name='vout')
        tid_vsip1 = self.get_wire_id('nch', row_in_index, 'ds', wire_name='vsip1')
        tid_vsip2 = self.get_wire_id('nch', row_in_index, 'ds', wire_name='vsip2')
        tid_cm = self.get_wire_id('nch', row_in_index, 'g', wire_name='cm')
        tid_IN = self.get_wire_id('nch', row_in_index, 'g', wire_name='IN')
        tid_dum = self.get_wire_id('nch', row_in_index, 'g', wire_name='dum')
        tid_vdiod = self.get_wire_id('pch', row_load_index, 'g', wire_name='vdiod')
        tid_sig1 = self.get_wire_id('pch', row_load_index, 'g', wire_name='sig1')
        tid_sw_s = self.get_wire_id('pch', row_load_index, 'ds', wire_name='sw')
        tid_sig_s = self.get_wire_id('pch', row_load_index, 'ds', wire_name='sig1')

        # ################################################################################
        # # 7:  Perform wiring
        # #
        # # Use the self.connect_to_tracks, self.connect_differential_tracks, self.connect_wires, etc
        # #  to perform connections
        # # Note that the drain/source/gate wire arrays of the transistors can be easily accessed as keys in the tx
        # # dictionary structure.
        # #
        # # Best practice:
        # #  - Avoid hard-coding widths and pitches
        # #    Instead, use TrackManger's get_width, get_space, or get_next_track functionality to make the design
        # #    fully portable across process
        # #  - Avoid hard-coding which layers connections will be on.
        # #    Instead use layers relative to self.mos_conn_layer
        # ################################################################################
        # # Define vertical tracks for the connections, based on the location of the col_stack_left/right

        grid = RoutingGridHelper(
            template_base=self,
            unit_mode=True,
            layer_id=vert_conn_layer,
            track_width=tr_manager.get_width(vert_conn_layer, 'sig1'),
        )

        tid_inv_g_vert = grid.track_by_col(col_idx=col_st3_left)
        tid_vdiod_vert = grid.track_by_col(col_idx=col_st1_right)
        tid_vg_cg_vert = grid.track_by_col(col_idx=col_st1_left - fg_space, half_track=True)
        tid_vs2_vert = grid.track_by_col(col_idx=col_st1_left + fg_st1 // 2, half_track=True)
        tid_vs1_vert = grid.track_by_col(col_idx=col_st1_right)
        tid_enb_vert = grid.track_by_col(col_idx=col_st3_left, offset=1)
        tid_vbias_vert = grid.track_by_col(col_idx=col_st2_left)

        # # # Connect input transistor drain to a horizontal track
        warr_en_p = self.connect_to_tracks(
            [inv_p.g],
            tid_sig1,
            min_len_mode=1
        )
        warr_en_n = self.connect_to_tracks(
            [inv_n.g, sw_en_n.g],
            tid_cm,
            min_len_mode=1
        )
        warr_enb_p = self.connect_to_tracks(
            [sw_en_p.g],
            tid_vdiod,
            min_len_mode=1
        )
        warr_en = self.connect_to_tracks(
            [warr_en_n, warr_en_p],
            tid_inv_g_vert,
            min_len_mode=1
        )

        warr_T_vg_cs = self.connect_to_tracks(
            [cs1_T.g, cs2_T.g, sw_en.d],
            tid_T_vg_cs,
            min_len_mode=1
        )
        warr_B_vg_cs = self.connect_to_tracks(
            [cs1_B.g, cs2_B.g],
            tid_B_vg_cs,
            min_len_mode=1
        )
        warr_vout = self.connect_to_tracks(
            [amp_n2.d, amp_n1.d, load_n.d],
            tid_vout,
            min_len_mode=1
        )
        warr_vdiod_d = self.connect_to_tracks(
            [amp_p2.d, amp_p1.d, load_p.d],
            tid_vout,
            min_len_mode=1
        )
        warr_vdiod_g = self.connect_to_tracks(
            [load_n.g, load_p.g],
            tid_vdiod,
            min_len_mode=1
        )
        warr_vdiod = self.connect_to_tracks(
            [warr_vdiod_d, warr_vdiod_g],
            tid_vdiod_vert,
            min_len_mode=1
        )

        warr_B_vs1 = self.connect_to_tracks(
            [cs1_B.d],
            tid_tail_B_vs1,
            min_len_mode=1
        )
        warr_B_vs2 = self.connect_to_tracks(
            [cs2_B.d],
            tid_tail_B_vs2,
            min_len_mode=1
        )
        warr_T_vs2 = self.connect_to_tracks(
            [cs2_T.d],
            tid_tail_T_vs,
            min_len_mode=1
        )
        warr_T_vs1 = self.connect_to_tracks(
            [cs1_T.d],
            tid_tail_T_vs,
            min_len_mode=1
        )
        warr_T_vs = self.connect_to_tracks(
            [cs_diod_T.d, cs_diod_B.d, cs_diod_T.g, cs_diod_B.g],
            tid_tail_T_vs,
            min_len_mode=1
        )
        warr_enb_inv = self.connect_to_tracks(
            [inv_n.d, inv_p.d],
            tid_vout,
            min_len_mode=1
        )
        warr_vbias_sw = self.connect_to_tracks(
            [sw_en_p.d, sw_en_n.d],
            tid_sig_s,
            min_len_mode=1
        )
        warr_vg_cs_swn = self.connect_to_tracks(
            [sw_en_n.s],
            tid_vout,
            min_len_mode=1
        )
        warr_vg_cs_swp = self.connect_to_tracks(
            [sw_en_p.s],
            tid_sw_s,
            min_len_mode=1
        )
        warr_vip1 = self.connect_to_tracks(
            [amp_p1.g],
            tid_dum,
            min_len_mode=1
        )
        warr_vip2 = self.connect_to_tracks(
            [amp_p2.g],
            tid_IN,
            min_len_mode=1
        )
        warr_vin1 = self.connect_to_tracks(
            [amp_n1.g],
            tid_cm,
            min_len_mode=1
        )
        warr_vin2 = self.connect_to_tracks(
            [amp_n2.g],
            tid_IN,
            min_len_mode=1
        )
        warr_vsip1 = self.connect_to_tracks(
            [amp_p1.s],
            tid_vsip1,
            min_len_mode=1
        )
        warr_vsip2 = self.connect_to_tracks(
            [amp_p2.s],
            tid_vsip1,
            min_len_mode=1
        )
        warr_vsin1 = self.connect_to_tracks(
            [amp_n1.s],
            tid_vsip2,
            min_len_mode=1
        )
        warr_vsin2 = self.connect_to_tracks(
            [amp_n2.s],
            tid_vsip2,
            min_len_mode=1
        )
        warr_enb_sw = self.connect_to_tracks(
            [sw_en.g],
            tid_vbias,
            min_len_mode=1
        )
        warr_vg_cs = self.connect_to_tracks(
            [warr_B_vg_cs, warr_T_vg_cs, warr_vg_cs_swn, warr_vg_cs_swp],
            tid_vg_cg_vert,
            min_len_mode=1
        )
        warr_vs1 = self.connect_to_tracks(
            [warr_B_vs1, warr_T_vs1],
            tid_vs1_vert,
            min_len_mode=1
        )
        warr_vs2 = self.connect_to_tracks(
            [warr_B_vs2, warr_T_vs2],
            tid_vs2_vert,
            min_len_mode=1
        )
        warr_enb = self.connect_to_tracks(
            [warr_enb_inv, warr_enb_sw, warr_enb_p],
            tid_enb_vert,
            min_len_mode=1
        )
        warr_vbias = self.connect_to_tracks(
            [warr_vbias_sw, warr_T_vs],
            tid_vbias_vert,
            min_len_mode=1
        )

        # # ################################################################################
        # # # 8:  Connections to substrate, and dummy fill
        # # #
        # # # - Use the self.connect_to_substrate method to perform wiring to the ntap and ptap
        # # #   where VDD and VSS will be
        # # # - Use self.fill_dummy() draw the dummy transistor structures, and finalize the VDD and VSS wiring
        # # #   - This method should be called last!
        # # ################################################################################
        # Connections to VSS
        self.connect_to_substrate(
            'ptap',
            [cs1_T.s, cs2_T.s, cs1_B.s, cs2_B.s, cs_diod_T.s, cs_diod_B.s, sw_en.s, inv_n.s]
        )
        #
        # Connections to VDD
        self.connect_to_substrate(
            'ntap',
            [load_n.s, load_p.s, inv_p.s]
        )
        #
        warr_vss, warr_vdd = self.fill_dummy()
        #
        # ################################################################################
        # # 9:  Add pins
        # #
        # ################################################################################
        self.add_pin('AVDD', warr_vdd, show=params.show_pins)

        self.add_pin('VSS', warr_vss, show=params.show_pins)
        self.add_pin('vsip1', warr_vsip1, show=params.show_pins)
        self.add_pin('vsip2', warr_vsip2, show=params.show_pins)
        self.add_pin('vsin1', warr_vsin1, show=params.show_pins)
        self.add_pin('vsin2', warr_vsin2, show=params.show_pins)
        self.add_pin('vip1', warr_vip1, show=params.show_pins)
        self.add_pin('vip2', warr_vip2, show=params.show_pins)
        self.add_pin('vin1', warr_vin1, show=params.show_pins)
        self.add_pin('vin2', warr_vin2, show=params.show_pins)
        self.add_pin('vs1', warr_B_vs1, show=params.show_pins)
        self.add_pin('vs2', warr_B_vs2, show=params.show_pins)
        self.add_pin('en', warr_en, show=params.show_pins)
        self.add_pin('vout', warr_vout, show=params.show_pins)
        self.add_pin('vbias', warr_vbias, show=params.show_pins)
        self.add_pin('vdiode', warr_vdiod, show=params.show_pins)

        ################################################################################
        # 10:  Organize parameters for the schematic generator
        #
        # To make the schematic generator very simple, organize the required transistor information (fins/widith,
        # intent, and number of fingers) into a convenient data structure
        # Finally set the self._sch_params property so that these parameters are accessible to the schematic generator
        ################################################################################
        # Define transistor properties for schematic
        tx_info = {}
        for tx in transistors:
            tx_info[tx.name] = {
                'w': tx.row.width,
                'th': tx.row.threshold,
                'fg': tx.fg
            }

        self._sch_params = dict(
            lch=params.lch,
            dum_info=self.get_sch_dummy_info(),
            tx_info=tx_info,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class cmfb(layout):
    """
    Class to be used as template in higher level layouts
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
