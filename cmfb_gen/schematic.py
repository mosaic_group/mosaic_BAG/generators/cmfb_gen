import os
from typing import *
from bag.design import Module

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/cmfb_templates', 'netlist_info', 'cmfb.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library cmfb_gen_templates cell cmfb

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        super().__init__(bag_config, yaml_file, parent=parent, prj=prj, **kwargs)

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            lch='channel length, in meters.',
            tx_info='List of dictionaries of transistor information',
            dum_info='Dummy information data structure.',
        )

    def design(self,
               lch: float,
               tx_info: Dict[str, Dict[str, Any]],
               dum_info: List[Tuple[Any]]):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """
        # design main transistors
        tran_info_list1 = [
            ('amp_n1', 'amp_n1'), ('amp_n2', 'amp_n2'),
            ('amp_p1', 'amp_p1'), ('amp_p2', 'amp_p2'),
            ('load_p', 'load_p'), ('load_n', 'load_n'),
            ('en_inv_pmos', 'inv_p'), ('en_inv_nmos', 'inv_n'),
            ('cs_diode_l', 'cs_diod_B'), ('cs_diode_r', 'cs_diod_T'),
            ('Sw_en_pmos', 'sw_en_p'), ('Sw_en_nmos', 'sw_en_n'),
            ('Sw_enb', 'sw_en'),
        ]

        tran_info_list2 = [
            ('cs1', 'cs1_B'), ('cs2', 'cs2_B'),

        ]

        for sch_name, layout_name in tran_info_list1:
            w = tx_info[layout_name]['w']
            th = tx_info[layout_name]['th']
            nf = tx_info[layout_name]['fg']
            self.instances[sch_name].design(w=w, l=lch, nf=nf, intent=th, multi=1)

        for sch_name, layout_name in tran_info_list2:
            w = tx_info[layout_name]['w']
            th = tx_info[layout_name]['th']
            nf = tx_info[layout_name]['fg']
            self.instances[sch_name].design(w=w, l=lch, nf=2 * nf, intent=th, multi=2)

        # design dummies
        self.design_dummy_transistors(dum_info, 'XDUMMY', 'AVDD', 'VSS')
