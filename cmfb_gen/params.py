#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *
from sal.testbench_base import TestbenchBase
from sal.testbench_params import *


@dataclass
class cmfb_layout_params(LayoutParamsBase):
    """
    Parameter class for cmfb_gen

    Args:
    ----
    ntap_w : Union[float, int]
        Width of the N substrate contact

    ptap_w : Union[float, int]
        Width of P substrate contact

    lch : float
        Channel length of the transistors

    th_dict : Dict[str, str]
        NMOS/PMOS threshold flavor dictionary

    fg_dum : int
        Number of fingers in NMOS transistor

    top_layer: int
        Top metal Layer used in Layout

    tr_spaces : Dict[str, int]
        Track spacing dictionary

    tr_widths : Dict[str, Dict[int, int]]
        Track width dictionary

    ndum_mid_stages : int
        number of dummy fingers/transistors between stages/columns of transistors

    w_dict : Dict[str, Union[float, int]]
        NMOS/PMOS width dictionary

    seg_dict : Dict[str, int]
        NMOS/PMOS number of segments dictionary

    guard_ring_nf : int
        Width of guard ring

    show_pins : bool
        True to create pin labels
    """

    ntap_w: Union[float, int]
    ptap_w: Union[float, int]
    lch: float
    th_dict: Dict[str, str]
    fg_dum: int
    top_layer: int
    tr_spaces: Dict[Union[str, Tuple[str, str]], Dict[int, Union[float, int]]]
    tr_widths: Dict[str, Dict[int, int]]
    ndum_mid_stages: int
    w_dict: Dict[str, Union[float, int]]
    seg_dict: Dict[str, int]
    guard_ring_nf: int
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> cmfb_layout_params:
        return cmfb_layout_params(
            ntap_w=10,
            ptap_w=10,
            lch=3 * min_lch,
            th_dict={
             'Dummy_NB': 'standard', 'Dummy_NT': 'standard',
             'Dummy_PB': 'standard', 'Dummy_PT': 'standard',
             'tail_T': 'standard', 'tail_B': 'standard',
             'In': 'standard', 'load': 'standard'
            },
            fg_dum=4,
            top_layer=5,
            tr_spaces={},
            tr_widths={'sig': {4: 1}},
            ndum_mid_stages=2,
            w_dict={
             'Dummy_NB': 20, 'Dummy_NT': 20,
             'Dummy_PB': 20, 'Dummy_PT': 20,
             'tail_T': 20, 'tail_B': 20,
             'In': 20, 'load': 20
            },
            seg_dict={'cs': 8, 'cs_diod': 4, 'amp': 2, 'load': 2, 'sw_en': 4, 'en_inv': 4},
            guard_ring_nf=0,
            show_pins=True,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> cmfb_layout_params:
        return cmfb_layout_params(
            ntap_w=10 * min_lch,
            ptap_w=10 * min_lch,
            lch=3 * min_lch,
            th_dict={
              'Dummy_NB': 'lvt', 'Dummy_NT': 'lvt',
              'Dummy_PB': 'lvt', 'Dummy_PT': 'lvt',
              'tail_T': 'lvt', 'tail_B': 'lvt',
              'In': 'lvt', 'load': 'lvt'
            },
            fg_dum=4,
            top_layer=5,
            tr_spaces={},
            tr_widths={'sig': {4: 1}},
            ndum_mid_stages=2,
            w_dict={
             'Dummy_NB': 20 * min_lch, 'Dummy_NT': 20 * min_lch,
             'Dummy_PB': 20 * min_lch, 'Dummy_PT': 20 * min_lch,
             'tail_T': 20 * min_lch, 'tail_B': 20 * min_lch,
             'In': 20 * min_lch, 'load': 20 * min_lch
            },
            seg_dict={'cs': 8, 'cs_diod': 4, 'amp': 2, 'load': 2, 'sw_en': 4, 'en_inv': 4},
            guard_ring_nf=2,
            show_pins=True,
        )


@dataclass
class cmfb_params(GeneratorParamsBase):
    layout_parameters: cmfb_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> cmfb_params:
        return cmfb_params(
            layout_parameters=cmfb_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
